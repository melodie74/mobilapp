import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { OuDanser } from '../ouDanser/ouDanser';
import { NotePage } from '../note/note';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tabBarElement: any;



// this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = HomePage;
  tab2Root: any = NotePage ;
  tab3Root: any = OuDanser;
  public noitems;

  constructor() {
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
    this.noitems = true;

  }

  ionViewWillEnter() {
    this.tab1Root.style.display = 'none';
    this.tab2Root.style.display = 'none';
    this.tab3Root.style.display = 'none';
  }

  ionViewWillLeave() {
    this.tab1Root.style.display = 'flex';
    this.tab2Root.style.display = 'flex';
    this.tab3Root.style.display = 'flex';
  }
}

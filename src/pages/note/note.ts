import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';
import {FormGroup, FormBuilder} from "@angular/forms";
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import {AProposPage} from "../a-propos/a-propos";


@Component({
  selector: 'page-about',
  templateUrl: 'note.html'
})
export class NotePage {



  formGroup : FormGroup;
  APropos: any = AProposPage;

  constructor(public navCtrl: NavController, private fb : FormBuilder) {


    const key = 'mobileNote';
    this.formGroup = fb.group({
      note : ['']
    });


   const currentText =  window.localStorage.getItem(key);

    this.formGroup.setValue({note :currentText});

    this.formGroup.get('note').valueChanges
        .distinctUntilChanged()
        .debounceTime(300)
        .subscribe(newText => window.localStorage.setItem(key,newText));

  }

}

import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';
import { Http } from "@angular/http";
import { Vibration } from 'ionic-native';
import { StatusBar, Splashscreen } from 'ionic-native';


import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/filter';
import {AProposPage} from "../a-propos/a-propos";



interface Video{
  url : SafeUrl;
  tag : string;
  isLocal : boolean;
  type : string;
}

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  recherche: String = null;
  videos: Video[];
  availableVideos: Video[];
  nbVideo: number = 0;
  APropos: any = AProposPage;


  constructor(public navCtrl: NavController, private http: Http, private domSanitizer: DomSanitizer) {


    this.availableVideos = [];

    this.initializeVideo();
    Splashscreen.hide();


  }

  panEvent($event) {
  }

  initializeVideo() {
    this.http.get('assets/videoList.json')
      .map(response => response.json())
      .do(data => data.forEach(video => video.url = this.domSanitizer.bypassSecurityTrustResourceUrl(video.url)))
      .subscribe(data => this.availableVideos = data);

  }

  onSearchUpdated(ev) {

    // set val to the value of the ev target
    const val = ev.target.value.trim();

    // if the value is an empty string don't filter the items
    if (val) {
      this.videos = this.availableVideos.filter((video) => {
        return (video.tag.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    }
    else {
      this.videos = [];
    }
    this.nbVideo= this.videos.length;
    if(this.videos.length==0) {
      Vibration.vibrate([1000]);
    }
  }
}




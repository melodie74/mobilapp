import { Component, ViewChild, ElementRef } from '@angular/core';
import { Geolocation } from 'ionic-native';

import {
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapsLatLng,
  CameraPosition,
  GoogleMapsMarkerOptions,
  GoogleMapsMarker
} from 'ionic-native';

import {AProposPage} from "../a-propos/a-propos";

import { NavController } from 'ionic-angular';
declare var google;

@Component({
  selector: 'page-contact',
  templateUrl: 'ouDanser.html'
})
export class OuDanser {
  APropos: any = AProposPage;
  private map: GoogleMap;
  constructor(public navCtrl: NavController) {}

// Load map only after view is initialize
  ngAfterViewInit() {
    this.loadMap();
  }


  loadMap() {

    // create a new map by passing HTMLElement
    let element: HTMLElement = document.getElementById('map');

    let map = new GoogleMap(element);

    this.map=map;

    // create LatLng object


    // listen to MAP_READY event
    map.one(GoogleMapsEvent.MAP_READY).then(() => {
      // move the map's camera to position



      Geolocation.getCurrentPosition().then((resp) => {

        // marqueur de notre position actuelle
        let ionic: GoogleMapsLatLng = new GoogleMapsLatLng(resp.coords.latitude, resp.coords.longitude);

        // position pour créer un marqueur
        let WallStreet: GoogleMapsLatLng = new GoogleMapsLatLng(45.568362, 5.922061);
        let rockingChair : GoogleMapsLatLng = new GoogleMapsLatLng(45.543592, 5.987885);
        // create CameraPosition
        let position: CameraPosition = {
          target: ionic,
          zoom: 15,
          tilt: 30
        };
        let markerOptions: GoogleMapsMarkerOptions = {
          position: ionic,
          title: 'vous êtes ici!'
        };
        let wallStreet: GoogleMapsMarkerOptions = {
          position: WallStreet,
          title: 'soiree tous les mardis !'
        };

        // création d'un marqueur
        let rocking: GoogleMapsMarkerOptions = {
          position: rockingChair,
          title: 'soiree tous les jeudis !'
        };

        // ajout du marqueur dans la map
        map.addMarker(markerOptions)
          .then((marker: GoogleMapsMarker) => {
            marker.showInfoWindow();
          });
        map.addMarker(wallStreet)
          .then((marker: GoogleMapsMarker) => {
            marker.showInfoWindow();
          });
        map.addMarker(rocking)
          .then((marker: GoogleMapsMarker) => {
            marker.showInfoWindow();
          });
        map.moveCamera(position); // works on iOS and Android
      }).catch((error) => {
        console.log('Error getting location', error);
      });
    });


    // create new marker

  }

}

import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { OuDanser } from '../pages/ouDanser/ouDanser';
import { NotePage } from '../pages/note/note';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import {AProposPage} from "../pages/a-propos/a-propos";


@NgModule({
  declarations: [
    MyApp,
    OuDanser,
    NotePage,
    HomePage,
    TabsPage,
    AProposPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    OuDanser,
    NotePage,
    HomePage,
    TabsPage,
    AProposPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
